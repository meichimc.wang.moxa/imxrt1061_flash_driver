/*
 * Copyright 2021 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "fsl_romapi.h"
#include "fsl_debug_console.h"
#include "fsl_cache.h"
#include "fsl_gpio.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "board.h"
#include "fsl_common.h"
#include "fsl_flexspi.h"
#include <FLASHPluginInterface.h>
#include <FLASHPluginConfig.h>

/*******************************************************************************
 * Define
 ******************************************************************************/
#define SPI_FLASH_CMD_RD_JEDEC_ID	0x9F
#define SPI_FLASH_CMD_4PP			0x38
//MXIC: 0x05, Winbond: 0x35
#define SPI_FLASH_CMD_GET_STATUS	0x05
#define SFDP_SIGNATRUE				0x50444653
const char vendor_str[2][20] = {"MACRONIX", "WINBOND"};
const uint8_t vendor_id[2] = { MACRONIX_ID, WINBOND_ID };

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void error_trap(void);
void app_finalize(void);
status_t FLEXSPI_NorFlash_GetVendorID(uint32_t instance, uint32_t *vendorID);
status_t FLEXSPI_NorFlash_Set_WEL(uint32_t instance);
//status_t FLEXSPI_NorFlash_Chip_Erase(uint32_t instance);
status_t FLEXSPI_NorFlash_SW_RESET(uint32_t instance);
void wdi_set(uint8_t output);
uint32_t flexspi_nor_wait_bus_busy(uint32_t instance);
status_t flexspi_nor_flash_page_quad_program(uint32_t instance, uint32_t dstAddr, const uint32_t *src);

/*******************************************************************************
 * Variables
 ******************************************************************************/

/*! @brief FLEXSPI NOR flash driver Structure */
static flexspi_nor_config_t norConfig;

/*******************************************************************************
 * Code
 ******************************************************************************/

status_t FLEXSPI_NorFlash_SW_RESET(uint32_t instance) {
	uint32_t lut_seq[4];
	uint32_t status;
	flexspi_xfer_t xfer;
	
	/* enable WEL */
	FLEXSPI_NorFlash_Set_WEL(instance);
	lut_seq[0] = FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x66, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0x0);
	ROM_FLEXSPI_NorFlash_UpdateLut(instance, NOR_CMD_LUT_SEQ_IDX_READID, (const uint32_t *)lut_seq, 1U);
	xfer.operation            = kFLEXSPIOperation_Command;
	xfer.seqId                = NOR_CMD_LUT_SEQ_IDX_READID;
	xfer.seqNum               = 1U;
	xfer.baseAddress          = 0U;
	xfer.isParallelModeEnable = false;
	status = ROM_FLEXSPI_NorFlash_CommandXfer(instance, &xfer);
	SDK_DelayAtLeastUs(10, SDK_DEVICE_MAXIMUM_CPU_CLOCK_FREQUENCY);
	
	lut_seq[0] = FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x99, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0x0);
	ROM_FLEXSPI_NorFlash_UpdateLut(instance, NOR_CMD_LUT_SEQ_IDX_READID, (const uint32_t *)lut_seq, 1U);
	xfer.operation            = kFLEXSPIOperation_Command;
	xfer.seqId                = NOR_CMD_LUT_SEQ_IDX_READID;
	xfer.seqNum               = 1U;
	xfer.baseAddress          = 0U;
	xfer.isParallelModeEnable = false;
	status = ROM_FLEXSPI_NorFlash_CommandXfer(instance, &xfer);
	
	SDK_DelayAtLeastUs(5000, SDK_DEVICE_MAXIMUM_CPU_CLOCK_FREQUENCY);
	
	return status;
}

status_t FLEXSPI_NorFlash_Set_WEL(uint32_t instance) {
	uint32_t lut_seq[4];
	uint32_t status;
	flexspi_xfer_t xfer;
	//set WEL bit in status register
	memset(lut_seq, 0, sizeof(lut_seq));
	lut_seq[0] = FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x06, kFLEXSPI_Command_STOP, kFLEXSPI_1PAD, 0x0);
	ROM_FLEXSPI_NorFlash_UpdateLut(instance, NOR_CMD_LUT_SEQ_IDX_READID, (const uint32_t *)lut_seq, 1U);
	xfer.operation            = kFLEXSPIOperation_Command;
	xfer.seqId                = NOR_CMD_LUT_SEQ_IDX_READID;
	xfer.seqNum               = 1U;
	xfer.baseAddress          = 0U;
	xfer.isParallelModeEnable = false;
	status = ROM_FLEXSPI_NorFlash_CommandXfer(instance, &xfer);
	return status;
}

status_t FLEXSPI_NorFlash_GetVendorID(uint32_t instance, uint32_t *vendorID)
{
	uint32_t lut_seq[4];
	memset(lut_seq, 0, sizeof(lut_seq));
	// Read manufacturer ID
	lut_seq[0] = FSL_ROM_FLEXSPI_LUT_SEQ(CMD_SDR, kFLEXSPI_1PAD, SPI_FLASH_CMD_RD_JEDEC_ID, READ_SDR, kFLEXSPI_1PAD, 4);
	ROM_FLEXSPI_NorFlash_UpdateLut(instance, NOR_CMD_LUT_SEQ_IDX_READID, (const uint32_t *)lut_seq, 1U);

	flexspi_xfer_t xfer;
	xfer.operation            = kFLEXSPIOperation_Read;
	xfer.seqId                = NOR_CMD_LUT_SEQ_IDX_READID;
	xfer.seqNum               = 1U;
	xfer.baseAddress          = 0U;
	xfer.isParallelModeEnable = false;
	xfer.rxBuffer             = vendorID;
	xfer.rxSize               = 3U;
	uint32_t status = ROM_FLEXSPI_NorFlash_CommandXfer(instance, &xfer);
	return status;
}

status_t FLEXSPI_NorFlash_Get_SFDP(uint32_t instance, uint32_t dstAddr, uint32_t num_bytes, uint32_t *rev) {
	uint32_t lut_seq[4];
	memset(lut_seq, 0, sizeof(lut_seq));

	lut_seq[0] = FLEXSPI_LUT_SEQ(kFLEXSPI_Command_SDR, kFLEXSPI_1PAD, 0x5A, kFLEXSPI_Command_RADDR_SDR, kFLEXSPI_1PAD, 0x18);
	lut_seq[1] = FLEXSPI_LUT_SEQ(kFLEXSPI_Command_DUMMY_SDR, kFLEXSPI_1PAD, 0x08, kFLEXSPI_Command_READ_SDR, kFLEXSPI_1PAD, num_bytes);
	ROM_FLEXSPI_NorFlash_UpdateLut(instance, NOR_CMD_LUT_SEQ_IDX_READID, (const uint32_t *)lut_seq, 1U);
	ROM_FLEXSPI_NorFlash_UpdateLut(instance, NOR_CMD_LUT_SEQ_IDX_READID, (const uint32_t *)lut_seq, 2U);
	flexspi_xfer_t xfer;
	xfer.operation            = kFLEXSPIOperation_Read;
	xfer.seqId                = NOR_CMD_LUT_SEQ_IDX_READID;
	xfer.seqNum               = 1U;
	xfer.baseAddress          = dstAddr;
	xfer.isParallelModeEnable = false;
	xfer.rxBuffer             = rev;
	xfer.rxSize               = num_bytes;
	uint32_t status = ROM_FLEXSPI_NorFlash_CommandXfer(instance, &xfer);
	return status;
}

/*
 * @brief Gets called when an error occurs.
 *
 * @details Print error message and trap forever.
 */
void error_trap(void)
{
	PRINTF("flash driver error trap ... \r\n");
	asm("bkpt 255");
}

struct FLASHBankInfo FLASHPlugin_Probe(unsigned base, unsigned size, unsigned width1, unsigned width2)
{
	struct FLASHBankInfo result = {
		.BaseAddress = base, 
		.BlockCount = norConfig.memConfig.sflashA1Size /  norConfig.sectorSize,
		.BlockSize = norConfig.sectorSize,
		.WriteBlockSize = MINIMUM_PROGRAMMED_BLOCK_SIZE
	};
	
	if (norConfig.pageSize != MINIMUM_PROGRAMMED_BLOCK_SIZE)
		error_trap();
	
	return result;
};

struct WorkAreaInfo FLASHPlugin_FindWorkArea(void *endOfStack)
{
	InterruptEnabler enabler;
    
	struct WorkAreaInfo info = { .Address = endOfStack, .Size = 0x8000 } ;
	return info ;
};

int FLASHPlugin_EraseSectors(unsigned firstSector, unsigned sectorCount)
{
	status_t status ;
	PRINTF("sector count = %d\r\n", sectorCount) ;
	PRINTF("Erase spi flash sectors from 0x%x to 0x%x\r\n",
	EXAMPLE_FLEXSPI_AMBA_BASE + firstSector * norConfig.sectorSize, 
	EXAMPLE_FLEXSPI_AMBA_BASE + firstSector * norConfig.sectorSize + sectorCount * norConfig.sectorSize - 1) ;
	if(sectorCount == (norConfig.memConfig.sflashA1Size / norConfig.sectorSize))
	{
		status = ROM_FLEXSPI_NorFlash_EraseAll(FlexSpiInstance, &norConfig) ;
	}
	else
	{
		status = ROM_FLEXSPI_NorFlash_Erase(FlexSpiInstance, &norConfig, firstSector * norConfig.sectorSize, sectorCount * norConfig.sectorSize) ;
	}
	//software reset
	FLEXSPI_NorFlash_SW_RESET(FlexSpiInstance) ;
	if(status != kStatus_Success)
	{
		error_trap() ;
	}
	else
	{
		PRINTF("Finish erasing the chip ... \r\n") ;
	}

	return sectorCount ;
}

int FLASHPlugin_Unload()
{
	return 0 ;
}

int FLASHPlugin_DoProgramSync(unsigned startOffset, const void *pData, int bytesToWrite)
{
	PRINTF("Program spi flash address from 0x%x to 0x%x\r\n", EXAMPLE_FLEXSPI_AMBA_BASE + startOffset, EXAMPLE_FLEXSPI_AMBA_BASE + startOffset + bytesToWrite - 1) ;
	int pages = bytesToWrite / FLASH_PAGE_SIZE ;

	for(int i = 0 ; i < pages ; i++)
	{
		status_t status = ROM_FLEXSPI_NorFlash_ProgramPage(FlexSpiInstance,
			&norConfig,
			startOffset + i * FLASH_PAGE_SIZE,
			(const uint32_t *)((const char *)pData + i  * FLASH_PAGE_SIZE)) ;
		
		if (status != kStatus_Success)
			return i * FLASH_PAGE_SIZE ;
	}
#ifdef WDI_EN
	GPIO_PinWrite(WDI_GPIO, WDI_GPIO_PIN, 0);
	SDK_DelayAtLeastUs(100000, SDK_DEVICE_MAXIMUM_CPU_CLOCK_FREQUENCY);
	GPIO_PinWrite(WDI_GPIO, WDI_GPIO_PIN, 1);
#endif
	return pages * FLASH_PAGE_SIZE ;
}

void wdi_set(uint8_t output)
{
	GPIO_PinWrite(WDI_GPIO, WDI_GPIO_PIN, output) ;
}

int main(void)
{
	status_t status;
	uint32_t i        = 0U;
	uint32_t vendorID = 0U;
	uint32_t device_id = 0U;
	uint32_t serialNorAddress; /* Address of the serial nor device location */
	uint32_t FlexSPISerialNorAddress; /* Address of the serial nor device in FLEXSPI memory */
	uint32_t serialNorTotalSize;
	uint32_t serialNorSectorSize;
	uint32_t serialNorPageSize;
	const char* vendor_name = "";
	
	/* config serial NOR option */
	serial_nor_config_option_t option;
	memset(&option, 0U, sizeof(option));
	option.option0.U = 0xc0000008U; //QuadSPI NOR, Frequency: 133MHz

	//BOARD_ConfigMPU(); //remove it because it is not requored for programming the memory
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitDebugConsole();
	PRINTF("\r\n\r\n******* MOXA MCU Programmer V0.0 *******\r\n");
	PRINTF("\r\nFLEXSPI NOR started!\r\n");
	/* Clean up FLEXSPI NOR flash driver Structure */
	memset(&norConfig, 0U, sizeof(flexspi_nor_config_t));

	/* Disable I cache */
	SCB_DisableICache();

	/* Setup FLEXSPI NOR Configuration Block */
	status = ROM_FLEXSPI_NorFlash_GetConfig(FlexSpiInstance, &norConfig, &option);
	if (status == kStatus_Success)
	{
		PRINTF("Successfully get FLEXSPI NOR configuration block\r\n");
	}
	else
	{
		PRINTF("\r\nGet FLEXSPI NOR configuration block failure!\r\n");
		error_trap();
	}

	/* Initializes the FLEXSPI module for the other FLEXSPI APIs */
	status = ROM_FLEXSPI_NorFlash_Init(FlexSpiInstance, &norConfig);
	if (status == kStatus_Success)
	{
		PRINTF("Successfully init FLEXSPI serial NOR flash\r\n");
	}
	else
	{
		PRINTF("\r\n Erase sector failure !\r\n");
		error_trap();
	}

	/* Perform software reset after initializing flexspi module */
	ROM_FLEXSPI_NorFlash_ClearCache(FlexSpiInstance);
	
	serialNorTotalSize  = norConfig.memConfig.sflashA1Size;
	serialNorSectorSize = norConfig.sectorSize;
	serialNorPageSize   = norConfig.pageSize; //program size

	/* Enable I cache */
	SCB_EnableICache();
	PRINTF("\r\n******* NOR flash Information *******\r\n");
	
	/*  Probe device presence by verifying Manufacturer ID */
	status = FLEXSPI_NorFlash_GetVendorID(FlexSpiInstance, &vendorID);
	for (i = 0; i < sizeof(vendor_id); i++)
	{
		if (((uint8_t)(vendorID & 0xFF)) == vendor_id[i])
		{
			vendor_name = vendor_str[i];
			break;
		}
	}
	PRINTF("Vendor ID: 0x%x %s\r\n", (vendorID & 0xFF), vendor_name);
	if (status == kStatus_Success)
	{
		device_id |= (vendorID & 0x00FF0000) >> 16;
		device_id |= vendorID & 0x0000FF00;
		PRINTF("Device ID: 0x%x\r\n", device_id);
	}
	else
	{
		PRINTF("\r\n Manufacture and device ID cannot be found!\r\n");
	}

#ifdef SFDP_INFO_EN
	// read sfdp
	uint32_t str_addr = 0x0;
	uint32_t rev_buf[16];
	memset(&rev_buf[0], 0, sizeof(rev_buf));
	FLEXSPI_NorFlash_Get_SFDP(FlexSpiInstance, str_addr, 20, &rev_buf[0]);
	if (rev_buf[0] == SFDP_SIGNATRUE)
	{
		PRINTF("SFDP signature:  0x%x\r\n", rev_buf[0]);
		PRINTF("SFDP header revision:  %x.%x\r\n", (rev_buf[1] & 0xFF00) >> 8, rev_buf[1] & 0xFF);
		PRINTF("Parameter revision:  %x.%x\r\n", (rev_buf[2] & 0xFF0000) >> 16, (rev_buf[2] & 0xFF00) >> 8);
	}
	str_addr = rev_buf[3] & 0xFFFFFF;
	memset(&rev_buf[0], 0, sizeof(rev_buf));
	FLEXSPI_NorFlash_Get_SFDP(FlexSpiInstance, str_addr, 64, &rev_buf[0]);
	PRINTF("\r\n****** SFDP table ******\r\n");
	for (i = 0; i < 16; i++) {
		PRINTF("SFDP[%d]: 0x%x\r\n", i, rev_buf[i]);
	}
#endif
	/* Print serial NOR flash information */
	PRINTF("Total program flash size:\t%d KB, Hex: (0x%x)\r\n", (serialNorTotalSize / 1024U), serialNorTotalSize);
	PRINTF("Program flash sector size:\t%d KB, Hex: (0x%x)\r\n", (serialNorSectorSize / 1024U), serialNorSectorSize);
	PRINTF("Program flash page size:\t%d B, Hex: (0x%x)\r\n\r\n", serialNorPageSize, serialNorPageSize);

	FLASHPlugin_InitDone();
	
	asm("bkpt 255");
	
	return 0;
}
