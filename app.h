/*
 * Copyright 2021 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#ifndef _APP_H_
#define _APP_H_

#include "fsl_clock.h"
#include "fsl_common.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
/*${macro:start}*/
#define EXAMPLE_FLEXSPI                 FLEXSPI
#define FLASH_SIZE_OPS                  0x2000 /* 64Mb/KByte */
#define EXAMPLE_FLEXSPI_AMBA_BASE       FlexSPI_AMBA_BASE
#define FLASH_PAGE_SIZE_OPS             256
#define EXAMPLE_SECTOR                  1 //20
#define SECTOR_SIZE                     0x1000 /* 4K */
#define EXAMPLE_FLEXSPI_CLOCK           kCLOCK_FlexSpi
#define FLASH_PORT                      kFLEXSPI_PortA1
#define EXAMPLE_FLEXSPI_RX_SAMPLE_CLOCK kFLEXSPI_ReadSampleClkLoopbackFromDqsPad

#define NOR_CMD_LUT_SEQ_IDX_READ_NORMAL_OPS        7
#define NOR_CMD_LUT_SEQ_IDX_READ_FAST_OPS          13
#define NOR_CMD_LUT_SEQ_IDX_READ_FAST_QUAD_OPS     0
#define NOR_CMD_LUT_SEQ_IDX_READSTATUS_OPS         1
#define NOR_CMD_LUT_SEQ_IDX_WRITEENABLE_OPS        2
#define NOR_CMD_LUT_SEQ_IDX_ERASESECTOR_OPS        3
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_SINGLE_OPS 6
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD_OPS   4
#define NOR_CMD_LUT_SEQ_IDX_READID_OPS             8
#define NOR_CMD_LUT_SEQ_IDX_WRITESTATUSREG_OPS     9
#define NOR_CMD_LUT_SEQ_IDX_ENTERQPI_OPS           10
#define NOR_CMD_LUT_SEQ_IDX_EXITQPI_OPS            11
#define NOR_CMD_LUT_SEQ_IDX_READSTATUSREG_OPS      12
#define NOR_CMD_LUT_SEQ_IDX_ERASECHIP_OPS          5
#define NOR_CMD_LUT_SEQ_IDX_READSFDP_OPS		   14

#define CUSTOM_LUT_LENGTH        60
#define FLASH_QUAD_ENABLE        0x40
#define FLASH_BUSY_STATUS_POL    1
#define FLASH_BUSY_STATUS_OFFSET 0

/*
 * If cache is enabled, this example should maintain the cache to make sure
 * CPU core accesses the memory, not cache only.
 */
#define CACHE_MAINTAIN 1

/*${macro:end}*/

/*******************************************************************************
 * Variables
 ******************************************************************************/
/*${variable:start}*/
#if (defined CACHE_MAINTAIN) && (CACHE_MAINTAIN == 1)
typedef struct _flexspi_cache_status
{
	volatile bool DCacheEnableFlag;
	volatile bool ICacheEnableFlag;
} flexspi_cache_status_t;
#endif
/*${variable:end}*/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
/*${prototype:start}*/
void BOARD_InitHardware(void);
static inline void flexspi_clock_init(void)
{
#if defined(XIP_EXTERNAL_FLASH) && (XIP_EXTERNAL_FLASH == 1)
	CLOCK_SetMux(kCLOCK_FlexspiMux, 0x2); /* Choose PLL2 PFD2 clock as flexspi source clock. 396M */
	CLOCK_SetDiv(kCLOCK_FlexspiDiv, 2); /* flexspi clock 133M. */
#else
	const clock_usb_pll_config_t g_ccmConfigUsbPll = { .loopDivider = 0U };
	CLOCK_InitUsb1Pll(&g_ccmConfigUsbPll);
	CLOCK_InitUsb1Pfd(kCLOCK_Pfd0, 24); /* Set PLL3 PFD0 clock 360MHZ. */
	CLOCK_SetMux(kCLOCK_FlexspiMux, 0x3); /* Choose PLL3 PFD0 clock as flexspi source clock. */
	CLOCK_SetDiv(kCLOCK_FlexspiDiv, 2); /* flexspi clock 120M. */
#endif
}
/*${prototype:end}*/

#endif /* _APP_H_ */
