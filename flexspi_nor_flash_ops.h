#ifndef _FLEXSPI_NOR_FLASH_OPS_H_
#define _FLEXSPI_NOR_FLASH_OPS_H_

#include "app.h"

status_t flexspi_nor_flash_erase_sector(FLEXSPI_Type *base, uint32_t address);
status_t flexspi_nor_flash_page_program(FLEXSPI_Type *base, uint32_t dstAddr, const uint32_t *src);
status_t flexspi_nor_get_vendor_id(FLEXSPI_Type *base, uint8_t *vendorId);
status_t flexspi_nor_enable_quad_mode(FLEXSPI_Type *base);
status_t flexspi_nor_erase_chip(FLEXSPI_Type *base);
void flexspi_nor_flash_init(FLEXSPI_Type *base);


#endif