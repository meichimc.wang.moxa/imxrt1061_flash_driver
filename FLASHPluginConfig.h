#pragma once

#define FlexSpiInstance									0U
#define EXAMPLE_FLEXSPI_AMBA_BASE						FlexSPI_AMBA_BASE
#define FLASH_SIZE										0x00800000L /* 8MBytes */
#define FLASH_PAGE_SIZE									256UL       /* 256Bytes */
#define FLASH_SECTOR_SIZE								0x1000UL   /* 4KBytes */
#define FLASH_BLOCK_SIZE								0x8000UL   /* 32KBytes */

#define EXAMPLE_FLEXSPI									FLEXSPI
#define CUSTOM_LUT_LENGTH								60
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD			4

#define FLASH_PORT										kFLEXSPI_PortA1

#define MINIMUM_PROGRAMMED_BLOCK_SIZE					FLASH_PAGE_SIZE
#define FLASH_PLUGIN_SUPPORT_ASYNC_PROGRAMMING			0
#define DEBUG_CONSOLE_UART								1
#define WDI_EN
#define MACRONIX_ID										0xC2
#define WINBOND_ID										0xEF